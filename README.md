## Fujitsu ESPRIMO C720

EFI-Folder for MacOS Catalina, BigSur, Monterey und Ventura

Intel Core i5-4590 Prozessor

For HD-4600 use
https://github.com/dortania/OpenCore-Legacy-Patcher

Opencore Bootloader
https://github.com/acidanthera/OpenCorePkg
**Pictures:** <br />
<br />
![Alt text](Pictures/Sonoma.png?raw=true "Previously")
<br />
![Alt text](Pictures/Startbildschierm.png?raw=true "Previously")